import {getConfig} from "../src/config";
import { writeFileSync, readFileSync } from "fs";
import { execSync } from "child_process";

async function _generateDocsForService(service: any) {

  return new Promise<void>(resolve => {
    
    let swaggerConfigPath = `./dist/docs/swagger-config.${service.serviceName}.json`;
    execSync("mkdir -p dist/docs");
    writeFileSync(swaggerConfigPath, _replaceArguments(swaggerConfigTemplate, service));
    execSync(`swaggerGen -c ${swaggerConfigPath}`);
    resolve();
    
  });

}

function _replaceArguments(templateString: string, args: any): string {

  let templateArgs = templateString.match(argumentRegex);
  let resultString = templateString.slice();

  if (templateArgs != null) {
    for (let templateArg of templateArgs) {

      let argumentKey = templateArg.substring(2, templateArg.length - 2);
      let argumentValue = args[argumentKey];

      resultString = resultString.replace(templateArg, argumentValue);

    }
  }

  return resultString;

}

let hosts = getConfig().runtime.hosts;
let services = [
  {
    serviceName: "mobile-api",
    appName: "Evans Francis",
    host: hosts["mobile-api"][getConfig().env]
  }
];

let swaggerConfigTemplate = readFileSync("./resources/swagger-config-template.json").toString();
let argumentRegex = /{{[a-zA-Z]+}}/gi;

Promise.all([
  services.map(async service => await _generateDocsForService(service))
]);