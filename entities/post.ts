import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("post" ,{schema:"ddschris_evans" } )
@Index("post_published_by_index",["published_by",])
export class post {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"title"
        })
    title:string;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"published_by"
        })
    published_by:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["Published","Unpublished"],
        name:"status"
        })
    status:string;
        
}
