import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gallery_images} from "./gallery_images";


@Entity("gallery" ,{schema:"ddschris_evans" } )
export class gallery {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"title"
        })
    title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"image"
        })
    image:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        

   
    @OneToMany(()=>gallery_images, (gallery_images: gallery_images)=>gallery_images.gallery,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    galleryImagess:gallery_images[];
    
}
