import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("chapters" ,{schema:"ddschris_evans" } )
export class chapters {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"language_id"
        })
    language_id:number | null;
        

    @Column("int",{ 
        nullable:false,
        name:"bible_id"
        })
    bible_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"chapter_no"
        })
    chapter_no:number;
        

    @Column("longtext",{ 
        nullable:true,
        name:"chapter_content"
        })
    chapter_content:string | null;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;
        
}
