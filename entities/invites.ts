import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("invites" ,{schema:"ddschris_evans" } )
export class invites {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"organization"
        })
    organization:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"title"
        })
    title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"email"
        })
    email:string;
        

    @Column("int",{ 
        nullable:false,
        name:"phone"
        })
    phone:number;
        

    @Column("longtext",{ 
        nullable:false,
        name:"address"
        })
    address:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"state"
        })
    state:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"city"
        })
    city:string;
        

    @Column("int",{ 
        nullable:false,
        name:"zip_code"
        })
    zip_code:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"website"
        })
    website:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"contact_person"
        })
    contact_person:string;
        

    @Column("int",{ 
        nullable:false,
        name:"contact_person_phone"
        })
    contact_person_phone:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"pastor"
        })
    pastor:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_title"
        })
    event_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_theme"
        })
    event_theme:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_date"
        })
    event_date:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"attendance"
        })
    attendance:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;
        
}
