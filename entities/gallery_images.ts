import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {gallery} from "./gallery";


@Entity("gallery_images" ,{schema:"ddschris_evans" } )
@Index("fk_galleryAlbum",["gallery",])
export class gallery_images {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>gallery, (gallery: gallery)=>gallery.galleryImagess,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'gallery_id'})
    gallery:gallery | null;


    @Column("varchar",{ 
        nullable:false,
        name:"image"
        })
    image:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
