import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("password_resets" ,{schema:"ddschris_evans" } )
@Index("password_resets_email_index",["email",])
export class password_resets {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"token"
        })
    token:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        
}
