import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("bible" ,{schema:"ddschris_evans" } )
export class bible {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:true,
        name:"language_id"
        })
    language_id:number | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bible_name"
        })
    bible_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"bible_image"
        })
    bible_image:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"bible_description"
        })
    bible_description:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
