import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("fire_form_data" ,{schema:"ddschris_evans" } )
export class fire_form_data {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"f_name"
        })
    f_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"m_name"
        })
    m_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"l_name"
        })
    l_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"phone"
        })
    phone:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"address"
        })
    address:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"amount"
        })
    amount:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
