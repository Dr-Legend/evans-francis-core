import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {audios} from "./audios";


@Entity("music_album" ,{schema:"ddschris_evans" } )
export class music_album {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"album_title"
        })
    album_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"album_image"
        })
    album_image:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        

   
    @OneToMany(()=>audios, (audios: audios)=>audios.album,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    audioss:audios[];
    
}
