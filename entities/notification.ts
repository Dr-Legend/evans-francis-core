import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("notification" ,{schema:"ddschris_evans" } )
export class notification {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:191,
        name:"device_id"
        })
    device_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fcm_id"
        })
    fcm_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"platform"
        })
    platform:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;
        
}
