import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("videos" ,{schema:"ddschris_evans" } )
export class videos {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"video_title"
        })
    video_title:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"youtube_video_id"
        })
    youtube_video_id:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:20,
        name:"sort_by_alphabet"
        })
    sort_by_alphabet:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"lyrics"
        })
    lyrics:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
