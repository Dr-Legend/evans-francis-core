import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("lyrics" ,{schema:"ddschris_evans" } )
export class lyrics {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"language_id"
        })
    language_id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"video_title"
        })
    video_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"poster"
        })
    poster:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"lyrics"
        })
    lyrics:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"video"
        })
    video:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;
        
}
