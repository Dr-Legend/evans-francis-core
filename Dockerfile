FROM node:10
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD ts-node src/index.mobile-api.ts
EXPOSE 3000