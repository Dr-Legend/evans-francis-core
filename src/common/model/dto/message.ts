import { Required } from '../../annotations';

export interface IMessage {
  id: number;
  title: string;
  image: string;
  created_at: Date;
  updated_at: Date;
}

export class MessageDto {

  public id: number;
  public title: string;
  public image: string;
  public created_at: Date;
  public updated_at: Date;

 constructor(poc: IMessage) {
   this.id = poc.id;
   this.title = poc.title;
   this.image = poc.image;
   this.created_at = poc.created_at;
   this.updated_at = poc.updated_at;
 }

}

export class CreateMessageRequestDto {

  @Required()
  public id:number
  public title: string;
  @Required()
  public image: string;


}