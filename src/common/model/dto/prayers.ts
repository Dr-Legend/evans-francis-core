import { Required } from '../../../common/annotations';

export interface IPrayer {
	 id: number;
	 title: string;
	 phone: number;
	 email: string;
	 city: string;
	 state: string;
	 country: string;
	 prayer_for: string;
	 message: string;
	 created_at: Date;
	 updated_at: Date;
}

export class PrayerDto {

	public id?: number;
	public title: string;
	public phone: number;
	public email: string;
	public city: string;
	public state: string;
	public country: string;
	public prayer_for: string;
	public message: string;
	public created_at: Date;
	public updated_at: Date;

	constructor(prayer: IPrayer) {
		this.id = prayer.id;
		this.title = prayer.title;
		this.phone = prayer.phone;
		this.email = prayer.email;
		this.city = prayer.city;
		this.state = prayer.state;
		this.country = prayer.country;
		this.prayer_for = prayer.prayer_for;
		this.message = prayer.message;
		this.created_at = prayer.created_at;
		this.updated_at = prayer.updated_at;
	}

}

export class CreatePrayerRequestDto {

  @Required()
  public title: string;
  @Required()
  public phone: number;
  @Required()
  public email: string;
  @Required()
	public city: string;
  @Required()
  public state: string;
  @Required()
  public country: string;
  @Required()
  public prayer_for: string;
  @Required()
  public message: string;

}