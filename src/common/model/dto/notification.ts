import { Required } from '../../annotations';

export interface INotification {
  id: number;
  fcm_id: string;
  platform: string;
  device_id: string;
  created_at: Date;
  updated_at: Date;
}

export class NotificationDto {

  public device_id: string;
  public fcm_id: string;
  public platform: string;
  public created_at: Date;
  public updated_at: Date;

 constructor(poc: INotification) {
   this.device_id = poc.device_id;
   this.fcm_id = poc.fcm_id;
   this.platform = poc.platform;
   this.created_at = poc.created_at;
   this.updated_at = poc.updated_at;
 }

}

export class CreateNotificationRequestDto {


  @Required()
  public device_id: string;
  @Required()
  public fcm_id: string;
  @Required()
  public platform: string;
  


}