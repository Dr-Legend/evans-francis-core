import { Required } from '../../annotations';

export interface IPof {
  id: number;
  f_name: string;
  m_name: string;
  l_name: string;
  phone: string;
  email: string;
  address: string;
  country: string;
  amount: string;
  created_at: Date;
  updated_at: Date;
}

export class PofDto {

  public id: number;
  public f_name: string;
  public m_name: string;
  public l_name: string;
  public phone: string;
  public email: string;
  public address: string;
  public country: string;
  public amount: string;
  public created_at: Date;
  public updated_at: Date;

 constructor(poc: IPof) {
   this.id = poc.id;
   this.f_name = poc.f_name;
   this.m_name = poc.m_name;
   this.l_name = poc.l_name;
   this.phone = poc.phone;
   this.email = poc.email;
   this.address = poc.address;
   this.country = poc.country;
   this.amount = poc.amount;
   this.created_at = poc.created_at;
   this.updated_at = poc.updated_at;
 }

}

export class CreatePofRequestDto {


  @Required()
  public f_name: string;

  public m_name: string;
  @Required()
  public l_name: string;
  @Required()
  public phone: number;
  @Required()
  public email: string;
  @Required()
  public country: string;
  @Required()
  public amount: string;


}