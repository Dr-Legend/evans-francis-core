import { Required } from '../../../common/annotations';

export interface IInvite {
	 id: number;
	 organization: string;
	 title: string;
	 email: string;
	 phone: number;
	 address: string;
	 country: string;
	 state: string;
	 city: string;
	 zip_code: number;
	 website: string;
	 contact_person: string;
	 contact_person_phone: number;
	 pastor: string;
	 event_title: string;
	 event_theme: string;
	 event_date: string;
	 attendance: string;
	 created_at: Date;
	 updated_at: Date;
}

export class InviteDto {

  public id: number;
  public organization: string;
  public title: string;
  public email: string;
  public phone: number;
  public address: string;
  public country: string;
  public state: string;
  public city: string;
  public zip_code: number;
  public website: string;
  public  contact_person: string;
  public  contact_person_phone: number;
   public pastor: string;
   public event_title: string;
   public  event_theme: string;
   public  event_date: string;
   public  attendance: string;
   public  created_at: Date;
   public updated_at: Date;

	constructor(invite: IInvite) {
		this.id = invite.id;
		this.organization = invite.organization;
		this.title = invite.title;
		this.email = invite.email;
		this.phone = invite.phone;
		this.address = invite.address;
		this.country = invite.country;
		this.state = invite.state;
		this.city = invite.city;
		this.zip_code = invite.zip_code;
		this.website = invite.website;
		this.contact_person = invite.contact_person;
		this.contact_person_phone = invite.contact_person_phone;
		this.pastor = invite.pastor;
		this.event_title = invite.event_title;
		this.event_theme = invite.event_theme;
		this.event_date = invite.event_date;
		this.attendance = invite.attendance;
		this.created_at = invite.created_at;
		this.updated_at = invite.updated_at;
	}

}

export class CreateInviteRequestDto {

 @Required()
 public organization: string;
 @Required()
 public title: string;
 @Required()
 public email: string;
 @Required()
 public phone: number;
 @Required()
 public address: string;
 @Required()
 public country: string;
 @Required()
 public state: string;
 @Required()
 public city: string;
 @Required()
 public zip_code: number;
 @Required()
 public website: string;
 @Required()
 public  contact_person: string;
 @Required()
 public  contact_person_phone: number;
 @Required()
  public pastor: string;
  @Required()
  public event_title: string;
  @Required()
  public  event_theme: string;
  @Required()
  public  event_date: string;
  @Required()
  public  attendance: string;

}