import { Required } from '../../annotations';

export interface IReachUs {
  id: number;
  name: string;
  phone: number;
  email: string;
  message: string;
  created_at: Date;
  updated_at: Date;
}

export class ReachUsDto {

  public id: number;
  public name: string;
  public phone: number;
  public email: string;
  public message: string;
  public created_at: Date;
  public updated_at: Date;

 constructor(poc: IReachUs) {
   this.id = poc.id;
   this.name = poc.name;
   this.phone = poc.phone;
   this.email = poc.email;
   this.message = poc.message;
   this.created_at = poc.created_at;
   this.updated_at = poc.updated_at;
 }

}

export class CreateReachUsRequestDto {


  @Required()
  public name: string;
  @Required()
  public phone: number;
  @Required()
  public email: string;
  @Required()
  public message: string;


}