import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { ReachUsDto } from '../dto/reach_us';


@Entity("reach_us" ,{schema:"ddschris_evans" } )
export class reach_us {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"name"
        })
    name:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"email"
        })
    email:string;
        

    @Column("int",{ 
        nullable:false,
        name:"phone"
        })
    phone:number;
        

    @Column("longtext",{ 
        nullable:false,
        name:"message"
        })
    message:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;

    public static create(dto: ReachUsDto): reach_us {

        const poc = new reach_us;
    
        if (dto.id)
        poc.id = dto.id;
        poc.name = dto.name;
        poc.phone = dto.phone;
        poc.email = dto.email;
        poc.message = dto.message;
        poc.created_at = dto.created_at;
        poc.updated_at = dto.updated_at;
        return poc;
    
      }
}
