import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { fire } from './fire';
import { PofDto } from '../dto/pof-s';


@Entity("fire_form_data" ,{schema:"ddschris_evans" } )
export class fire_form_data {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"f_name"
        })
    f_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"m_name"
        })
    m_name:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"l_name"
        })
    l_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"phone"
        })
    phone:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"address"
        })
    address:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"amount"
        })
    amount:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
    public static create(dto: PofDto): fire_form_data {

        const pof = new fire_form_data;
    
        if (dto.id)
        pof.id = dto.id;
        pof.f_name = dto.f_name;
        pof.m_name = dto.m_name;
        pof.l_name = dto.l_name;
        pof.phone = dto.phone;
        pof.email = dto.email;
        pof.address = dto.address;
        pof.country = dto.country;
        pof.amount = dto.amount;
        pof.created_at = dto.created_at;
        pof.updated_at = dto.updated_at;
        return pof;
    
      }
}
