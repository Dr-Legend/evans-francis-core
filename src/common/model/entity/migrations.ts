import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("migrations" ,{schema:"ddschris_evans" } )
export class migrations {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"migration"
        })
    migration:string;
        

    @Column("int",{ 
        nullable:false,
        name:"batch"
        })
    batch:number;
        
}
