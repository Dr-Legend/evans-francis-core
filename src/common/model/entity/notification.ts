import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { NotificationDto } from '../dto/notification';

@Entity("notification" ,{schema:"ddschris_evans" } )
export class notification {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:191,
        name:"device_id"
        })
    device_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"fcm_id"
        })
    fcm_id:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"platform"
        })
    platform:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
    public static create(dto: NotificationDto): notification {

        const notifi = new notification;
        notifi.device_id = dto.device_id;
        notifi.fcm_id = dto.fcm_id;
        notifi.platform = dto.platform;
        notifi.created_at = dto.created_at;
        notifi.updated_at = dto.updated_at;
        return notifi;
    
      }
}

