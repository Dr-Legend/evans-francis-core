import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("discussion" ,{schema:"ddschris_evans" } )
export class discussion {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"title"
        })
    title:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"image"
        })
    image:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"description"
        })
    description:string;
        

    @Column("date",{ 
        nullable:false,
        name:"date"
        })
    date:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"substr_date"
        })
    substr_date:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
