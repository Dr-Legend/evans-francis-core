import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {music_album} from "./music_album";


@Entity("audios" ,{schema:"ddschris_evans" } )
@Index("fk_musicAlbum",["album",])
export class audios {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>music_album, (music_album: music_album)=>music_album.audios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'album_id'})
    album:music_album | null;


    @Column("varchar",{ 
        nullable:false,
        name:"audio_title"
        })
    audio_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"image"
        })
    image:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"audio_file"
        })
    audio_file:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
