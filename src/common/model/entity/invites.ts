import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { InviteDto } from '../dto/invite';


@Entity("invites" ,{schema:"ddschris_evans" } )
export class invites {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:true,
        name:"organization"
        })
    organization:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        name:"title"
        })
    title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"email"
        })
    email:string;
        

    @Column("int",{ 
        nullable:false,
        name:"phone"
        })
    phone:number;
        

    @Column("longtext",{ 
        nullable:false,
        name:"address"
        })
    address:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"state"
        })
    state:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"city"
        })
    city:string;
        

    @Column("int",{ 
        nullable:false,
        name:"zip_code"
        })
    zip_code:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"website"
        })
    website:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"contact_person"
        })
    contact_person:string;
        

    @Column("int",{ 
        nullable:false,
        name:"contact_person_phone"
        })
    contact_person_phone:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"pastor"
        })
    pastor:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_title"
        })
    event_title:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_theme"
        })
    event_theme:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"event_date"
        })
    event_date:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"attendance"
        })
    attendance:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;
        
    public static create(dto: InviteDto): invites {

        const invite = new invites;
    
        if (dto.id)
          invite.id = dto.id;
          invite.organization = dto.organization;
        invite.title = dto.title;
        invite.email = dto.email;
        invite.phone = dto.phone;
        invite.address = dto.address;
        invite.country = dto.country;
        invite.state = dto.state;
        invite.city = dto.city;
        invite.zip_code = dto.zip_code;
        invite.website = dto.website;
        invite.contact_person = dto.contact_person;
        invite.contact_person_phone = dto.contact_person_phone;
        invite.pastor = dto.pastor;
        invite.event_title = dto.event_title;
        invite.event_theme = dto.event_theme;
        invite.event_date = dto.event_date;
        invite.attendance = dto.attendance;
        invite.created_at = dto.created_at;
        invite.updated_at = dto.updated_at;
        return invite;
    
      }
}
