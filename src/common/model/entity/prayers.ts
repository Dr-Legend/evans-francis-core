import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { PrayerDto, IPrayer } from "../dto/prayers";


@Entity("prayers" ,{schema:"ddschris_evans" } )
export class prayers {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"title"
        })
    title:string;
        

    @Column("int",{ 
        nullable:false,
        name:"phone"
        })
    phone:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"city"
        })
    city:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"state"
        })
    state:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"prayer_for"
        })
    prayer_for:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"message"
        })
    message:string;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"created_at"
        })
    created_at:Date;
        

    @Column("timestamp",{ 
        nullable:false,
        default: () => "'0000-00-00 00:00:00'",
        name:"updated_at"
        })
    updated_at:Date;

    public constructor();
  public constructor(prayer: IPrayer);
  public constructor(prayer?: IPrayer) {
    if (prayer) {
        this.id = prayer.id;
		this.title = prayer.title;
		this.phone = prayer.phone;
		this.email = prayer.email;
		this.city = prayer.city;
		this.state = prayer.state;
		this.country = prayer.country;
		this.prayer_for = prayer.prayer_for;
		this.message = prayer.message;
		this.created_at = prayer.created_at;
		this.updated_at = prayer.updated_at;
    }
  }
    public static create(dto: PrayerDto): prayers {

        const prayer = new prayers;
    
        if (dto.id)
          prayer.id = dto.id;
        prayer.title = dto.title;
        prayer.phone = dto.phone;
        prayer.email = dto.email;
        prayer.city = dto.city;
        prayer.state = dto.state;
        prayer.country = dto.country;
        prayer.prayer_for = dto.prayer_for;
        prayer.message = dto.message;
        prayer.created_at = dto.created_at;
        prayer.updated_at = dto.updated_at;
        return prayer;
    
      }
        
}
