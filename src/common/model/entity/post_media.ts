import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("post_media" ,{schema:"ddschris_evans" } )
@Index("post_media_post_id_index",["post_id",])
export class post_media {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        

    @Column("int",{ 
        nullable:false,
        unsigned: true,
        name:"post_id"
        })
    post_id:number;
        

    @Column("enum",{ 
        nullable:false,
        enum:["Youtube","Other"],
        name:"key"
        })
    key:string;
        

    @Column("longtext",{ 
        nullable:false,
        name:"value"
        })
    value:string;
        
}
