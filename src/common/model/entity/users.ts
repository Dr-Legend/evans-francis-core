import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("users" ,{schema:"ddschris_evans" } )
@Index("users_email_unique",["email",],{unique:true})
@Index("users_api_token_unique",["api_token",],{unique:true})
@Index("users_user_type_index",["user_type",])
@Index("users_status_index",["status",])
export class users {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"f_name"
        })
    f_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"l_name"
        })
    l_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"password"
        })
    password:string;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'User'",
        enum:["Admin","User","SubAdmin"],
        name:"user_type"
        })
    user_type:string;
        

    @Column("varchar",{ 
        nullable:true,
        unique: true,
        length:191,
        name:"api_token"
        })
    api_token:string | null;
        

    @Column("int",{ 
        nullable:true,
        name:"country_id"
        })
    country_id:number | null;
        

    @Column("enum",{ 
        nullable:false,
        default: () => "'Inactive'",
        enum:["Active","Inactive"],
        name:"status"
        })
    status:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:100,
        name:"remember_token"
        })
    remember_token:string | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
