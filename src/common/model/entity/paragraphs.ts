import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";


@Entity("paragraphs" ,{schema:"ddschris_evans" } )
export class paragraphs {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"bible_id"
        })
    bible_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"chapter_id"
        })
    chapter_id:number;
        

    @Column("int",{ 
        nullable:false,
        name:"chapter_no"
        })
    chapter_no:number;
        

    @Column("int",{ 
        nullable:false,
        name:"paragraph_no"
        })
    paragraph_no:number;
        

    @Column("longtext",{ 
        nullable:false,
        name:"chapter_content"
        })
    chapter_content:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;
        
}
