import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import { PocDto } from '../dto/poc';


@Entity("cloud_form_data" ,{schema:"ddschris_evans" } )
export class cloud_form_data {

    @PrimaryGeneratedColumn({
        type:"int", 
        unsigned: true,
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"f_name"
        })
    f_name:string;
        

    @Column("varchar",{ 
        nullable:true,
        length:191,
        name:"m_name"
        })
    m_name:string | null;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"l_name"
        })
    l_name:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"phone"
        })
    phone:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"address"
        })
    address:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"country"
        })
    country:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:191,
        name:"prayer_time"
        })
    prayer_time:string;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("timestamp",{ 
        nullable:true,
        name:"updated_at"
        })
    updated_at:Date | null;

    public static create(dto: PocDto): cloud_form_data {

        const poc = new cloud_form_data;
    
        if (dto.id)
        poc.id = dto.id;
        poc.f_name = dto.f_name;
        poc.m_name = dto.m_name;
        poc.l_name = dto.l_name;
        poc.phone = dto.phone;
        poc.email = dto.email;
        poc.address = dto.address;
        poc.country = dto.country;
        poc.prayer_time = dto.prayer_time;
        poc.created_at = dto.created_at;
        poc.updated_at = dto.updated_at;
        return poc;
    
      }
        
}
