import { blogs } from "../../../common/model/entity/blogs";
export interface IBlogRepository {
  archivesDates(): Promise<blogs[]>;
	getBlogs(monthString:string): Promise<blogs[]>;
	
}