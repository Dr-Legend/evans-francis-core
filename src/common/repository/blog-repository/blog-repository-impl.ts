import {BaseRepository} from "../base-repository";
import { IBlogRepository } from '.';
import { Repository, Entity, FindOptionsUtils } from 'typeorm';
import { blogs } from '../../../common/model/entity/blogs';


export class BlogRepositoryImpl extends BaseRepository implements IBlogRepository{
  private _blogsRepository: Repository<blogs> = this.connection.getRepository(blogs);
  getBlogs(monthString:string): Promise<blogs[]> {
    return this._blogsRepository.find({
      where:{
        substr_date: monthString
      },
      order:{
        date:'DESC'
      }
    });
  }
  archivesDates(): Promise<any> {
    return this._blogsRepository.findAndCount({ select:['substr_date'],     order:{
      date:'ASC'
    }});
  }
}