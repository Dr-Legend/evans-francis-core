
import { messages } from '../../model/entity/messages';

export interface IMessagesRepository {

	getMessages(): Promise<messages[]>;
	
}