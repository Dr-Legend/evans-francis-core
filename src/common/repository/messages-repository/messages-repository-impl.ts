import {BaseRepository} from "../base-repository";
import { IMessagesRepository } from '.';
import { Repository } from 'typeorm';
import { messages } from '../../model/entity/messages';



export class MessagesRepositoryImpl extends BaseRepository implements IMessagesRepository{
  private _MessagesRepository: Repository<messages> = this.connection.getRepository(messages);
  getMessages(): Promise<messages[]> {
    return this._MessagesRepository.find(
      { 
        take: 10,
        order:{
          created_at: 'DESC'
        }
    }
    );
  }
}