import {BaseRepository} from "../base-repository";
import { IPocRepository } from '.';
import { Repository } from 'typeorm';
import { cloud_form_data } from '../../model/entity/cloud_form_data';





export class PocRepositoryImpl extends BaseRepository implements IPocRepository{

  private _prayersRepository: Repository<cloud_form_data> = this.connection.getRepository(cloud_form_data);

  public async createPoc(poc: cloud_form_data): Promise<cloud_form_data> {
    return await this._prayersRepository.save(poc);
  }
  


}