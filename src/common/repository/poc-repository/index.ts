

import { youtube_videos } from "common/model/entity/youtube_videos";
import { PrayerDto } from '../../model/dto/prayers';
import { cloud_form_data } from '../../../common/model/entity/cloud_form_data';


export interface IPocRepository {
	createPoc(pof:cloud_form_data): Promise<cloud_form_data>;
	
}