import {BaseRepository} from "../base-repository";
import { IReachUsRepository } from '.';
import { Repository } from 'typeorm';
import { reach_us } from '../../model/entity/reach_us';





export class ReachUsRepositoryImpl extends BaseRepository implements IReachUsRepository{

  private reachUsRepository: Repository<reach_us> = this.connection.getRepository(reach_us);

  public async createReachUs(reachUs: reach_us): Promise<reach_us> {
    return await this.reachUsRepository.save(reachUs);
  }
  


}