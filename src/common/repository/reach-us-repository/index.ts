

import { youtube_videos } from "common/model/entity/youtube_videos";
import { PrayerDto } from '../../model/dto/prayers';
import { reach_us } from '../../model/entity/reach_us';


export interface IReachUsRepository {
	createReachUs(pof:reach_us): Promise<reach_us>;
	
}