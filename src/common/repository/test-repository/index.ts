


export interface ITestRepository {

	// createInvoice(order: Order, items?: OrderItem[], weight?: number): Promise<Invoice>;
	// getInvoicesByOrderId(orderId: number): Promise<Invoice[]>;
	getInvoices(): Promise<string>;
	
}