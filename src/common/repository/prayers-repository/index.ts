
import { prayers } from '../../model/entity/prayers';


export interface IPrayersRepository {
	createPrayer(prayer:prayers): Promise<prayers>;
	
}