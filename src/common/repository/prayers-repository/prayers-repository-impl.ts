import {BaseRepository} from "../base-repository";
import { IPrayersRepository } from '.';
import { Repository } from 'typeorm';
import { prayers } from '../../model/entity/prayers';




export class PrayersRepositoryImpl extends BaseRepository implements IPrayersRepository{

  private _prayersRepository: Repository<prayers> = this.connection.getRepository(prayers);

  public async createPrayer(invite: prayers): Promise<prayers> {
    return await this._prayersRepository.save(invite);
  }
  


}