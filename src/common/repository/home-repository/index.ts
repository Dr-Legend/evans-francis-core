

import { home_images } from '../../../common/model/entity/home_images';

export interface IHomeImagesRepository {

	getHomeImages(): Promise<home_images[]>;
	
}