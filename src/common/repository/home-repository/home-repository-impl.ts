import {BaseRepository} from "../base-repository";
import { IHomeImagesRepository } from '.';
import { Repository } from 'typeorm';
import { home_images } from '../../model/entity/home_images';



export class HomeImageRepositoryImpl extends BaseRepository implements IHomeImagesRepository{
  private _homeImagesRepository: Repository<home_images> = this.connection.getRepository(home_images);
  getHomeImages(): Promise<home_images[]> {
    return this._homeImagesRepository.find({ 
      order:{
        created_at: 'DESC'
      }
  });
  }
}