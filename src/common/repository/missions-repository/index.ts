
import { missions } from '../../model/entity/missions';

export interface IMissionsRepository {

	getMissions(): Promise<missions[]>;
	
}