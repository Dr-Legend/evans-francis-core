import {BaseRepository} from "../base-repository";
import { IMissionsRepository } from '.';
import { Repository } from 'typeorm';
import { missions } from '../../model/entity/missions';



export class MissionRepositoryImpl extends BaseRepository implements IMissionsRepository{
  private _missionsRepository: Repository<missions> = this.connection.getRepository(missions);
  getMissions(): Promise<missions[]> {
    return this._missionsRepository.find({ 
      order:{
        created_at: 'DESC'
      }
  });
  }
}