import {BaseRepository} from "../base-repository";
import { IGalleryRepository } from '.';
import { Repository } from 'typeorm';
import { gallery } from '../../model/entity/gallery';


export class GalleryRepositoryImpl extends BaseRepository implements IGalleryRepository{
  private _galleryRepository: Repository<gallery> = this.connection.getRepository(gallery);
  getGallery(): Promise<gallery[]> {
   return this._galleryRepository.find({
     relations:["galleryImages"],
     order:{
       created_at:'DESC'
     }
   });
  }
 

}