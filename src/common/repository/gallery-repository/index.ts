import { gallery } from "common/model/entity/gallery";


export interface IGalleryRepository {
	getGallery(): Promise<gallery[]>;
	
}