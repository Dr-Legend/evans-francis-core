import {BaseRepository} from "../base-repository";
import { IMusicRepository } from '.';
import { Repository } from 'typeorm';
import { music_album } from '../../model/entity/music_album';


export class MusicRepositoryImpl extends BaseRepository implements IMusicRepository{
  private _musicRepository: Repository<music_album> = this.connection.getRepository(music_album);
  getAlbums(): Promise<music_album[]> {
   return this._musicRepository.find({
     relations:["audios"],
     order:{
       created_at:'DESC'
     }
   });
  }
 

}