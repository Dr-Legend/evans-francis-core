import { music_album } from "common/model/entity/music_album";


export interface IMusicRepository {
	getAlbums(): Promise<music_album[]>;
	
}