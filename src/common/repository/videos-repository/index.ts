

import { youtube_videos } from "common/model/entity/youtube_videos";


export interface IVideoRepository {
  archivesDates(): Promise<youtube_videos[]>;
	getVideos(monthString:string): Promise<youtube_videos[]>;
	
}