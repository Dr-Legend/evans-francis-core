import {BaseRepository} from "../base-repository";
import { IVideoRepository } from '.';
import { Repository } from 'typeorm';
import { youtube_videos } from '../../model/entity/youtube_videos';




export class VideosRepositoryImpl extends BaseRepository implements IVideoRepository{

  private _videoRepository: Repository<youtube_videos> = this.connection.getRepository(youtube_videos);
  
  getVideos(monthString:string): Promise<youtube_videos[]> {
    return this._videoRepository.find({
      where:{
        substr_date: monthString
      },
      order:{
        date:'DESC'
      }
    });
  }
  archivesDates(): Promise<any> {
    return this._videoRepository.findAndCount({ select:['substr_date'],     order:{
      date:'ASC'
    }});
  }

}