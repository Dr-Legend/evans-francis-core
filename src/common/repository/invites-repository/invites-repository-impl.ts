import {BaseRepository} from "../base-repository";
import { IInvitesRepository } from '.';
import { Repository } from 'typeorm';
import { invites } from '../../model/entity/invites';




export class InvitesRepositoryImpl extends BaseRepository implements IInvitesRepository{

  private _invitesRepository: Repository<invites> = this.connection.getRepository(invites);

  public async createInvite(invite: invites): Promise<invites> {
    return await this._invitesRepository.save(invite);
  }
  


}