
import { prayers } from '../../model/entity/prayers';
import { invites } from '../../model/entity/invites';


export interface IInvitesRepository {
	createInvite(invite:invites): Promise<invites>;
	
}