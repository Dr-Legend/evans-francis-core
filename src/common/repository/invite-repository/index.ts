

import { youtube_videos } from "../../../common/model/entity/youtube_videos";
import { PrayerDto } from '../../model/dto/prayers';
import { prayers } from '../../../common/model/entity/prayers';


export interface IPrayersRepository {
	createPrayer(prayer:prayers): Promise<prayers>;
	
}