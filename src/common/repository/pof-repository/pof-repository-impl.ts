import {BaseRepository} from "../base-repository";
import { IPofRepository } from '.';
import { Repository } from 'typeorm';
import { cloud_form_data } from '../../model/entity/cloud_form_data';
import { fire_form_data } from '../../../common/model/entity/fire_form_data';
import { fire } from '../../../common/model/entity/fire';





export class PofRepositoryImpl extends BaseRepository implements IPofRepository{

  private _pofRepository: Repository<fire_form_data> = this.connection.getRepository(fire_form_data);

  public async createPof(pof: fire_form_data): Promise<fire_form_data> {
    return await this._pofRepository.save(pof);
  }
  


}