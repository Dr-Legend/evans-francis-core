

import { youtube_videos } from "common/model/entity/youtube_videos";
import { PrayerDto } from '../../model/dto/prayers';

import { fire_form_data } from '../../../common/model/entity/fire_form_data';


export interface IPofRepository {
	createPof(pof:fire_form_data): Promise<fire_form_data>;
	
}