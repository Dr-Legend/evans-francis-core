
import { Connection } from "typeorm";
import { Database } from "../db";
import { ITestRepository } from "./test-repository";
import { TestRepositoryImpl } from "./test-repository/test-repository-impl";
import { IBlogRepository } from "./blog-repository";
import { BlogRepositoryImpl } from "./blog-repository/blog-repository-impl";
import { IMusicRepository } from "./music-repository";
import { MusicRepositoryImpl } from "./music-repository/music-repository-impl";
import { IVideoRepository } from "./videos-repository";
import { VideosRepositoryImpl } from "./videos-repository/videos-repository-impl";
import { IGalleryRepository } from "./gallery-repository";
import { GalleryRepositoryImpl } from "./gallery-repository/gallery-repository-impl";
import { IPrayersRepository } from "./prayers-repository";
import { PrayersRepositoryImpl } from "./prayers-repository/prayers-repository-impl";
import { IPocRepository } from "./poc-repository";
import { PocRepositoryImpl } from "./poc-repository/poc-repository-impl";
import { IPofRepository } from "./pof-repository";
import { PofRepositoryImpl } from "./pof-repository/pof-repository-impl";
import { IInvitesRepository } from './invites-repository';
import { InvitesRepositoryImpl } from './invites-repository/invites-repository-impl';
import { IMissionsRepository, IEventsRepository, INotificationsRepository, IMessagesRepository, IReachUsRepository } from '.';
import { MissionRepositoryImpl } from './missions-repository/missions-repository-impl';
import { EventRepositoryImpl } from './events-repository/events-repository-impl';
import { NotificationsRepositoryImpl } from './notifications-repository/notifications-repository-impl';
import { IHomeImagesRepository } from './home-repository';
import { HomeImageRepositoryImpl } from './home-repository/home-repository-impl';
import { MessagesRepositoryImpl } from './messages-repository/messages-repository-impl';
import { ReachUsRepositoryImpl } from './reach-us-repository/reach-us-repository-impl';

export class RepositoryFactory {
  private static _instance: RepositoryFactory;

  public static get instance(): RepositoryFactory {
    if (!this._instance) {
      this._instance = new RepositoryFactory(Database.getConnection());
    }
    return this._instance;
  }

  public constructor(private _connection: Connection) {}

  public get testRepository(): ITestRepository {
    return new TestRepositoryImpl(this._connection);
  }
  public get blogsRepository(): IBlogRepository {
    return new BlogRepositoryImpl(this._connection);
  }
  public get musicRepository(): IMusicRepository {
    return new MusicRepositoryImpl(this._connection);
  }
  public get videosRepository(): IVideoRepository {
    return new VideosRepositoryImpl(this._connection);
  }
  public get galleryRepository(): IGalleryRepository {
    return new GalleryRepositoryImpl(this._connection);
  }
  public get prayersRepository(): IPrayersRepository {
    return new PrayersRepositoryImpl(this._connection);
  }
  public get pocRepository(): IPocRepository {
    return new PocRepositoryImpl(this._connection);
  }
  public get pofRepository(): IPofRepository {
    return new PofRepositoryImpl(this._connection);
  }
  public get invitesRepository(): IInvitesRepository {
    return new InvitesRepositoryImpl(this._connection);
  }
  public get missionsRepository(): IMissionsRepository {
    return new MissionRepositoryImpl(this._connection);
  }
  public get eventsRepository(): IEventsRepository {
    return new EventRepositoryImpl(this._connection);
  }
  public get notificationsRepository(): INotificationsRepository {
    return new NotificationsRepositoryImpl(this._connection);
  }
  public get homeImagesRepository(): IHomeImagesRepository {
    return new HomeImageRepositoryImpl(this._connection);
  }
  public get messagesRepository(): IMessagesRepository {
    return new MessagesRepositoryImpl(this._connection);
  }
  public get reachUsRepository(): IReachUsRepository {
    return new ReachUsRepositoryImpl(this._connection);
  }
}
