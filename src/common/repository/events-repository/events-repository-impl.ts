import {BaseRepository} from "../base-repository";
import { IEventsRepository } from '.';
import { Repository } from 'typeorm';
import { events } from '../../model/entity/events';


export class EventRepositoryImpl extends BaseRepository implements IEventsRepository{
  private _eventsRepository: Repository<events> = this.connection.getRepository(events);
  getEvents(): Promise<events[]> {
    return this._eventsRepository.find({ 
      order:{
        created_at: 'DESC'
      }
  });
  }
}