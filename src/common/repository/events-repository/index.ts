
import { events } from '../../../common/model/entity/events';
export interface IEventsRepository {

	getEvents(): Promise<events[]>;
	
}