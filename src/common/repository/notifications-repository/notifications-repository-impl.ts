import {BaseRepository} from "../base-repository";
import { INotificationsRepository } from '.';
import { Repository, UpdateResult } from 'typeorm';
import { notification } from '../../../common/model/entity/notification';
import * as gcm from 'node-gcm';


export class NotificationsRepositoryImpl extends BaseRepository implements INotificationsRepository{
 
  private _notificationRepository: Repository<notification> = this.connection.getRepository(notification);

  public async createNotifications(notifi: notification): Promise<notification> {
    return await this._notificationRepository.save(notifi);
  }
  public async updateNotifications(notifi: notification): Promise<UpdateResult> {
   return await this._notificationRepository
    .createQueryBuilder()
    .update(notification)
    .set({ fcm_id: notifi.fcm_id })
    .where("device_id = :id", { id: notifi.device_id })
    .execute();
    // return await this._notificationRepository.update({device_id: notifi.device_id},notifi);
  }
  getNotification(id: string): Promise<notification | undefined> {
    return this._notificationRepository.findOne({
      where:{
        device_id: id
      }
    });
    
  }
  sendNotification(): string{
    var sender = new gcm.Sender('AAAAzmTRDSo:APA91bEjjkqWHL52H_RldAzEgsakk4aTVeD8RRVbQi20rB2Px6JYY_oXZH1vvf_e_Tt4EkluIgznGftV3PpRIfnhNFcON1W7UiinV9d-pOZVsV1vVKHyX7H0NfA9fjK3doTjg2NxIhUm');
    // Prepare a message to be sent
    var message = new gcm.Message({
      notification: {
        title: "Hello, World",
        icon: "ic_launcher",
        badge: "1",
        body: "This is a notification that will be displayed if your app is in the background.",
        click_action: 'FLUTTER_NOTIFICATION_CLICK',
      },
      data: { 
        click_action: 'FLUTTER_NOTIFICATION_CLICK',
         id: '1',
         status: 'done',
         screen:'/blogScreen'
       }
    });

    // Specify which registration IDs to deliver the message to
    var regTokens = ['cHxrasT0bqk:APA91bFdXkyj6Hgu4GncxcmdKU_m4qIcmRgCI5wwgn5bab0zXY4tzOt5_E69sue5YEcljwpB77kDH_H_BlaIkRf6Sc_7Fg5XeuuHy5PJ3XvqHh-B5Q4yK-CQPsyDOQR0Q5Pt5he4q2Dm'];

    // Actually send the message
    sender.send(message, { registrationTokens: regTokens }, function (err, response) {
      if (err) console.error(err);
      else console.log(response);
    });

    return "Promise(d)";
  }

  


}