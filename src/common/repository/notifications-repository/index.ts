
import { notification } from '../../../common/model/entity/notification';
import { UpdateResult } from 'typeorm';


export interface INotificationsRepository {
	createNotifications(notifi:notification): Promise<notification>;
	updateNotifications(notifi:notification): Promise<UpdateResult>;
	getNotification(id: string): Promise<notification | undefined>;
	sendNotification(): string;
}