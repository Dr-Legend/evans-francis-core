import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IMessagesRepository } from "../../common/repository/messages-repository";
import { messages } from '../../common/model/entity/messages';

@Produces("application/json")
@Tags("Messages")
@Path("/messages")
export class MessageController extends BaseController {
  private _messagesrepo: IMessagesRepository =
  RepositoryFactory.instance.messagesRepository;

  @JSONEndpoint
  @GET
  public async getMessages(): Promise<messages[]> {
    var something = this._messagesrepo.getMessages();
    console.log(something);
    return something;
  }
}