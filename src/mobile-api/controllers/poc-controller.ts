import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";
import { Tags, Produces, Security } from "typescript-rest-swagger";

import { PocDto, CreatePocRequestDto } from "../../common/model/dto/poc";
import { IPocRepository } from '../../common/repository/poc-repository';
import { prayers } from '../../common/model/entity/prayers';
import { cloud_form_data } from '../../common/model/entity/cloud_form_data';



@Produces("application/json")
@Tags("Pillar Of Cloud")
@Path("/pillar-of-cloud")
export class PocController extends BaseController {

	private _pocRepository: IPocRepository = RepositoryFactory.instance.pocRepository;

	@JSONEndpoint
	@POST
	public async createPoc(@JSONBody(CreatePocRequestDto) request: PocDto): Promise<cloud_form_data> {
    let poc = cloud_form_data.create(request);
		poc = await this._pocRepository.createPoc(poc);
		return poc;
  }

}