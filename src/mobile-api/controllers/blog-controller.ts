import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IBlogRepository } from "../../common/repository/blog-repository";
import { blogs } from '../../common/model/entity/blogs';

@Produces("application/json")
@Tags("Blogs")
@Path("/blogs")
export class BlogController extends BaseController {
  private _blogsrepo: IBlogRepository =
  RepositoryFactory.instance.blogsRepository;

  @JSONEndpoint
  @GET
  @Path("/blogs-by-month/:month")
  public async getBlogs(@PathParam("month") month: string): Promise<blogs[]> {
    var something = this._blogsrepo.getBlogs(month);
    console.log(something);
    return something;
  }

  @JSONEndpoint
  @GET
  @Path("/archive-dates")
  public async getArchivesDates(): Promise<blogs[]> {
    var something = this._blogsrepo.archivesDates();

    return something;
  }
}