import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IMusicRepository } from "../../common/repository/music-repository";
import { music_album } from '../../common/model/entity/music_album';

@Produces("application/json")
@Tags("Music Albums")
@Path("/music-albums")
export class MusicController extends BaseController {
  private _musicrepo: IMusicRepository =
  RepositoryFactory.instance.musicRepository;
  
  @JSONEndpoint
  @GET
  public async getMusic(): Promise<music_album[]> {
    var something = this._musicrepo.getAlbums();
    console.log(something);
    return something;
  }
}