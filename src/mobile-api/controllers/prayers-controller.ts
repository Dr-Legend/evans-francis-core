import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";

import { Tags, Produces, Security } from "typescript-rest-swagger";
import { PrayerDto, CreatePrayerRequestDto } from "../../common/model/dto/prayers";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";


import { getConfig } from "../../config";
import { IPrayersRepository } from '../../common/repository/prayers-repository';
import { prayers } from '../../common/model/entity/prayers';



@Produces("application/json")
@Tags("Prayers")
@Path("/prayer")
export class PrayersController extends BaseController {

	private _prayersRepository: IPrayersRepository = RepositoryFactory.instance.prayersRepository;

	@JSONEndpoint
	@POST
	public async createPrayer(@JSONBody(CreatePrayerRequestDto) request: PrayerDto): Promise<PrayerDto> {
    let prayer = prayers.create(request);
		prayer = await this._prayersRepository.createPrayer(prayer);
		return new PrayerDto(prayer);
  }

}