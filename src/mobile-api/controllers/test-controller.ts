import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { ITestRepository } from '../../common/repository';

@Produces("application/json")
@Tags("Test")
@Path("/test")
export class Test extends BaseController {
  private _testrepo: ITestRepository =
    RepositoryFactory.instance.testRepository;

  @JSONEndpoint
  @GET
  public async test(): Promise<string> {
    // var something = this._testrepo.getInvoices();
    // console.log(something);
    return "Route for promocode Created..";
  }
}
