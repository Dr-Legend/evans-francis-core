import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IMissionsRepository } from "../../common/repository/missions-repository";
import { missions } from '../../common/model/entity/missions';

@Produces("application/json")
@Tags("Missions")
@Path("/missions")
export class MissionController extends BaseController {
  private _missionsrepo: IMissionsRepository =
  RepositoryFactory.instance.missionsRepository;

  @JSONEndpoint
  @GET
  public async getMissions(): Promise<missions[]> {
    var something = this._missionsrepo.getMissions();
    console.log(something);
    return something;
  }
}