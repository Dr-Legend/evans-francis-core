import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";
import { Tags, Produces, Security } from "typescript-rest-swagger";

import { ReachUsDto, CreateReachUsRequestDto } from "../../common/model/dto/reach_us";
import { IReachUsRepository } from '../../common/repository/reach-us-repository';
import { prayers } from '../../common/model/entity/prayers';
import { cloud_form_data } from '../../common/model/entity/cloud_form_data';
import { reach_us } from '../../common/model/entity/reach_us';



@Produces("application/json")
@Tags("Reach Us")
@Path("/reach-us")
export class ReachUsController extends BaseController {

	private _reachUsRepository: IReachUsRepository = RepositoryFactory.instance.reachUsRepository;

	@JSONEndpoint
	@POST
	public async createReachUs(@JSONBody(CreateReachUsRequestDto) request: ReachUsDto): Promise<reach_us> {
    let reachUs = reach_us.create(request);
		reachUs = await this._reachUsRepository.createReachUs(reachUs);
		return reachUs;
  }

}