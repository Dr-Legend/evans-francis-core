import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IVideoRepository } from "../../common/repository/videos-repository";
import { youtube_videos } from '../../common/model/entity/youtube_videos';

@Produces("application/json")
@Tags("Videos")
@Path("/videos")
export class VideosController extends BaseController {
  private _videosrepo: IVideoRepository =
  RepositoryFactory.instance.videosRepository;

  @JSONEndpoint
  @GET
  @Path("videos/:month")
  public async getVideos(@PathParam("month") month: string): Promise<youtube_videos[]> {
    var something = this._videosrepo.getVideos(month);
    console.log(something);
    return something;
  }

  @JSONEndpoint
  @GET
  @Path("/archive-dates")
  public async getArchivesDates(): Promise<youtube_videos[]> {
    var something = this._videosrepo.archivesDates();

    return something;
  }
}