import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IEventsRepository } from "../../common/repository/events-repository";
import { events } from '../../common/model/entity/events';

@Produces("application/json")
@Tags("Events")
@Path("/events")
export class EventController extends BaseController {
  private _eventsrepo: IEventsRepository =
  RepositoryFactory.instance.eventsRepository;

  @JSONEndpoint
  @GET
  public async getEvents(): Promise<events[]> {
    var something = this._eventsrepo.getEvents();
    console.log(something);
    return something;
  }
}