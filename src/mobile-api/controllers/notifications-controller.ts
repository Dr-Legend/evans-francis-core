import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET, PUT, PathParam} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";
import { Tags, Produces, Security } from "typescript-rest-swagger";

import { NotificationDto, CreateNotificationRequestDto } from "../../common/model/dto/notification";
import { INotificationsRepository } from '../../common/repository/notifications-repository';
import { prayers } from '../../common/model/entity/prayers';
import { cloud_form_data } from '../../common/model/entity/cloud_form_data';
import { notification } from '../../common/model/entity/notification';
import { UpdateResult } from 'typeorm';



@Produces("application/json")
@Tags("Device")
@Path("/add-device")
export class NotificationController extends BaseController {

	private _notificationRepository: INotificationsRepository = RepositoryFactory.instance.notificationsRepository;

	@JSONEndpoint
  @GET
  @Path(":id")
  public async getBlogs(@PathParam("id") id: string): Promise<notification | undefined> {
    var something = this._notificationRepository.getNotification(id);
    console.log(something);
    return something;
	}
	
	@JSONEndpoint
	@POST
	public async createNotification(@JSONBody(CreateNotificationRequestDto) request: NotificationDto): Promise<notification> {
    let notifi = notification.create(request);
		notifi = await this._notificationRepository.createNotifications(notifi);
		return notifi;
  }
	@JSONEndpoint
	@PUT
	public async updateNotification(@JSONBody(CreateNotificationRequestDto) request: NotificationDto): Promise<UpdateResult> {
    let notifi = notification.create(request);
		var updated = await this._notificationRepository.updateNotifications(notifi);
		return updated;
  }
	@JSONEndpoint
	@GET
	@Path("/send")
	public async sendNotification(): Promise<string> {
		var result = this._notificationRepository.sendNotification();
		return 'Notification sent!';
  }


}