import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IHomeImagesRepository } from "../../common/repository/home-repository";
import { home_images } from '../../common/model/entity/home_images';

@Produces("application/json")
@Tags("Home Images")
@Path("/home-images")
export class HomeImageController extends BaseController {
  private _home_imagesrepo: IHomeImagesRepository =
  RepositoryFactory.instance.homeImagesRepository;

  @JSONEndpoint
  @GET
  public async getHomeImages(): Promise<home_images[]> {
    var something = this._home_imagesrepo.getHomeImages();
    console.log(something);
    return something;
  }
}