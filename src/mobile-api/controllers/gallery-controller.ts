import { Produces, Tags } from 'typescript-rest-swagger';
import { Path, GET, PathParam } from 'typescript-rest';
import { BaseController } from '../../common/controller/base-controller';
import { RepositoryFactory } from '../../common/repository/factory';
import { JSONEndpoint } from '../../common/annotations';
import { IGalleryRepository } from "../../common/repository/gallery-repository";
import { gallery } from '../../common/model/entity/gallery';

@Produces("application/json")
@Tags("Gallery Albums")
@Path("/gallery-albums")
export class GalleryController extends BaseController {
  private _galleryrepo: IGalleryRepository =
  RepositoryFactory.instance.galleryRepository;
  
  @JSONEndpoint
  @GET
  public async getGallery(): Promise<gallery[]> {
    var something = this._galleryrepo.getGallery();
    console.log(something);
    return something;
  }
}