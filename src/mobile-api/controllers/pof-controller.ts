import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";
import { Tags, Produces, Security } from "typescript-rest-swagger";

import { PofDto, CreatePofRequestDto } from "../../common/model/dto/pof-s";
import { IPofRepository } from '../../common/repository/pof-repository';
import { prayers } from '../../common/model/entity/prayers';
import { cloud_form_data } from '../../common/model/entity/cloud_form_data';
import { fire_form_data } from '../../common/model/entity/fire_form_data';



@Produces("application/json")
@Tags("Pillar Of Fire")
@Path("/pillar-of-fire")
export class PofController extends BaseController {

	private _pofRepository: IPofRepository = RepositoryFactory.instance.pofRepository;

	@JSONEndpoint
	@POST
	public async createPof(@JSONBody(CreatePofRequestDto) request: PofDto): Promise<fire_form_data> {
    let pof = fire_form_data.create(request);
		pof = await this._pofRepository.createPof(pof);
		return pof;
  }

}