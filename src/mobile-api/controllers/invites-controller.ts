import {BaseController} from "../../common/controller/base-controller";
import {Path, POST, GET} from "typescript-rest";
import { JSONEndpoint} from "../../common/annotations";

import { Tags, Produces, Security } from "typescript-rest-swagger";
import { InviteDto, CreateInviteRequestDto } from "../../common/model/dto/invite";
import { JSONBody } from "../../common/annotations/json-body";
import { RepositoryFactory } from "../../common/repository/factory";


import { getConfig } from "../../config";
import { IInvitesRepository } from '../../common/repository/invites-repository';
import { invites } from '../../common/model/entity/invites';



@Produces("application/json")
@Tags("Invites")
@Path("/invite")
export class InvitesController extends BaseController {

	private _invitesRepository: IInvitesRepository = RepositoryFactory.instance.invitesRepository;

	@JSONEndpoint
	@POST
	public async createInvite(@JSONBody(CreateInviteRequestDto) request: InviteDto): Promise<invites> {
    let invite = invites.create(request);
		invite = await this._invitesRepository.createInvite(invite);
		return invite;
  }

}